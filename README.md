# cs373-idb-gcp

This is an introductory tutorial shows how one can run a simple flask app with templates. It also deploys the app on GCP.

Do NOT forget to include:
- "venv" inside ".gitignore" to prevent pushing virtual environment to remote repo
- "requirements.txt"

By default, Flask dev server runs on localhost, i.e., 127.0.0.1, but you can change that
by speciftying "host='0.0.0.0'" within your "app.run()". In this case, the server will
run on your machine's IP address.

#After executing "python main.py", you need to delete the contents of the table "book"; 
otherwise you receive an error.
$ psql -U postgres -h localhost
Password for user postgres:

#connect to bookdb
postgres=# \c bookdb
WARNING: Console code page (437) differs from Windows code page (1252)
         8-bit characters might not work correctly. See psql reference
         page "Notes for Windows users" for details.
You are now connected to database "bookdb" as user "postgres".
#drop the table book
bookdb=# drop table book;



